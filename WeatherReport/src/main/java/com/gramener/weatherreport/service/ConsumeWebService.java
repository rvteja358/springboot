package com.gramener.weatherreport.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.gramener.weatherreport.RestTemplate;



@Service
public class ConsumeWebService {
	 boolean umbrella;
	 JSONObject weatherReport;
	@Value("${openwhethermap.url}")
	private String currentWhetherUrl;
	@Value("${openwhethermap.subscriptionkey}")
	private String subscriptionkey;
	@Autowired
	RestTemplate restTemplate;
	public JSONObject getData(String location) {
		
		try {
			
	 String finalURL=currentWhetherUrl+location+",uk&APPID="+subscriptionkey;
	  weatherReport = new JSONObject();	 
     String forObject = restTemplate.getForObject(finalURL, String.class);
     
     JSONObject responseJson = new JSONObject(forObject);
     
     JSONArray weatherJsonArray = responseJson.getJSONArray("weather");
    
     JSONObject idJSONObject = weatherJsonArray.getJSONObject(0);
    int weatherconditioncode=(int) idJSONObject.get("id");
    // 2XX:Thunderstorm; 3xx:Drizzle; 5xx: Rain
     if(weatherconditioncode >= 200 && weatherconditioncode <= 531) {
    	  umbrella=true;
     }else {
    	  umbrella=false;
     }
     JSONObject mainJsonArr = responseJson.getJSONObject("main");
    
     weatherReport.put("temp", mainJsonArr.get("temp"));
     weatherReport.put("pressure", mainJsonArr.get("pressure"));
     weatherReport.put("umbrella", umbrella);
		}catch(Exception e) {
			System.err.println(e.getMessage());
			 weatherReport.put("Message","city not found");
		}
     return weatherReport;
	}
   

}
